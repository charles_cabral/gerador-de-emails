# Emails Automáticos - Marabraz

Workflow para geração de templates de emails automáticos da loja

O que ele faz?

1. Cria emails HTML a partir de templates e components
2. Compila SCSS para CSS
3. Insere inline o estilo do arquivo `inline.css` diretamente no html e incorpora o arquivo `embedded.css` no head
4. Gera uma pré-visualização dos emails


## Getting Started


#### 1. Dependências

Este workflow possui as seguintes dependências:

- [Node](https://nodejs.org/en/)
- [Gulp](https://gulpjs.com/)


#### 2. Instalando os pacotes

``` html
npm install
```


#### 3. Rodando o workflow

```
gulp
```


O html dos emails compilados estarão no diretório `build/`.
E poderão ser visualizados no navegador em `http://localhost:8000`