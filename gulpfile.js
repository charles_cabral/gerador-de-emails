const gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    scss = require('postcss-scss'),
    autoprefixer = require('autoprefixer'),
    inlineCss = require('gulp-inline-css'),
    nunjucksRender = require('gulp-nunjucks-render'),
    htmlbeautify = require('gulp-html-beautify'),
    data = require('gulp-data'),
    replace = require('gulp-replace'),
    connect = require('gulp-connect');

// Helpers
const postcssProcessors = [
        autoprefixer( { browsers: ['last 2 versions', 'ie > 10'] } )
    ],
    globalData = {
        data: require('./src/data/global.json')
    },
    filesToWatch = [
        'src/sass/**/*.scss',
        'src/emails/*.nunjucks',
        'src/templates/**/*.nunjucks',
        'src/data/*.json'
    ]


// CSS
gulp.task('sassInline', (callback) => {
    return gulp.src('src/sass/inline.scss')
        .pipe(
           postcss(postcssProcessors, {syntax: scss})
        )
        .pipe(
            sass({ outputStyle: 'expanded' })
            .on('error', gutil.log)
        )
        .pipe(gulp.dest('build/css/'))
})

gulp.task('inlinecss', ['sassInline', 'nunjucks'], () => {
    return gulp.src('build/*.html')
        .pipe(
            inlineCss({
                applyStyleTags: false,
                removeStyleTags: false
            })
            .on('error', gutil.log)
        )
        .pipe(gulp.dest('build/'))
        .pipe(connect.reload())
})


// TEMPLATING
gulp.task('nunjucks', () => {
    return gulp.src('src/emails/*.nunjucks')
        .pipe(
            data(function() {
                return globalData;
            })
            .on('error', gutil.log)
        )
        .pipe(
            nunjucksRender({
                path: ['src/templates/', 'build/css/']
            })
            .on('error', gutil.log)
        )
        .pipe(htmlbeautify({indentSize: 4}))
        .pipe(replace('&quot;', '"'))
        .pipe(replace('&#39;', '"'))
        .pipe(gulp.dest('build/'))
})

gulp.task('connect', () => {
    connect.server({
        port: 8000,
        root: 'build',
        livereload: true
    })
})

gulp.task('watch', () => {
    gulp.watch(filesToWatch,['nunjucks', 'inlinecss']);
})

gulp.task('default', ['connect', 'nunjucks', 'inlinecss', 'watch']);


